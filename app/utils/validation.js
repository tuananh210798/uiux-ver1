/* eslint-disable no-bitwise */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import moment from 'moment';
export const validateTextField = (
  value,
  rules = {
    minLength: 0,
    maxLength: 250,
    isRequired: false,
    allowSpecialChar: false,
    isNumber: false,
  },
) => {
  if (!value && !rules.isRequired) return true;
  if (typeof value !== 'string') return false;
  const str = value.trim();
  if (str.length <= 0) return false;
  // const regex = /^[a-z0-9A-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/;
  if (Number.isInteger(rules.minLength) && str.length < rules.minLength)
    return false;
  if (Number.isInteger(rules.maxLength) && str.length > rules.maxLength)
    return false;
  const regex = /[!@#$%^&*()~<>]/;
  if (rules.allowSpecialChar) return true;
  if (rules.isNumber) {
    return /^[0-9]+$/.test(str);
  }
  return !regex.test(str);
};
export const validateViewConfig = (
  value,
  rules = {
    minLength: 0,
    maxLength: 250,
    isRequired: false,
    allowSpecialChar: false,
    isNumber: false,
  },
) => {
  if (!value && !rules.isRequired) return true;
  if (typeof value !== 'string') return false;
  const str = value.trim();
  if (str.length <= 0) return false;
  // const regex = /^[a-z0-9A-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/;
  if (Number.isInteger(rules.minLength) && str.length < rules.minLength)
    return false;
  if (Number.isInteger(rules.maxLength) && str.length > rules.maxLength)
    return false;
  const regex = /[!@#$%^&*~<>]/;
  if (rules.allowSpecialChar) return true;
  if (rules.isNumber) {
    return /^[0-9]+$/.test(str);
  }
  return !regex.test(str);
};

// valid list item code [number, text, _]
export const validateCode = value => {
  if (typeof value !== 'string') return false;
  const str = value.trim();
  if (!str.trim()) return false;
  const regex = /^[0-9A-Z_]+$/;
  return regex.test(str);
};

export const validateUserNameLogin = (
  value,
  rules = {
    minLength: 5,
    maxLength: 20,
  },
) => {
  const str = value || '';
  if (str.length > rules.maxLength)
    return `Độ dài giá trị nhập vào phải nhỏ hơn ${rules.maxLength}`;
  if (str.length < rules.minLength)
    return `Độ dài giá trị nhập vào phải lớn hơn ${rules.minLength}`;
  const regex = /^[0-9a-z]+$/;
  const testSpaceRegex = /\s/;
  if (!regex.test(str) || testSpaceRegex.test(str)) {
    return `Chỉ được phép nhập chữ cái không dấu, số`;
  }
  return null;
};

// validate dd/mm/yyyy
export const validateDate = (
  value,
  options = { dateRange: 5, dateRangeField: 'year' },
  format = 'DD/MM/YYYY',
  sep = '/',
) => {
  if (typeof value !== 'string') return false;
  const str = value.trim();
  if (!str.trim()) return false;
  const values = value.split(sep);
  const formats = format.split(sep);
  if (values.length !== formats.length) return false;
  for (let i = 0; i < values.length; i += 1) {
    if (values[i].length !== formats[i].length) return false;
  }
  // hardcord min year and max length for user input
  const minYear = moment().get(options.dateRangeField) - options.dateRange;
  const yearInt = parseInt(values[2], 10);
  if (yearInt < minYear) return false;
  return moment(value, format).isValid();
};
// validate number > 0
export const validateNumber = value => !Number.isNaN(value);

// validate email
export const validateEmail = value => {
  const rex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,}))$/;
  return rex.test(value);
};

// validate password
export const validatePassword = value => {
  // const rex = /^(?=.*[a-z])(?=.*[AZ])(?=.*[!@#\$%\^&\*])(?=^.{8,32}$)/;
  const rex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])(\S{8,}$)/;
  return rex.test(value);
};

// validate role code
export const validateRoleCode = value => {
  const rex = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[-_.,?!@#$%^&*()+=;:'"|])(\S{3,10}$)/;
  return rex.test(value);
};
// validate username
export const validateUserName = value => {
  const rex = /^[a-z0-9]{5,20}$/;
  return rex.test(value);
};
export const validateOrgs = (
  value,
  rules = {
    minLength: 3,
    maxLength: 250,
  },
) => {
  const str = value || '';
  if (str.length > rules.maxLength) return false;
  if (str.length < rules.minLength) return false;
  const regex = /^[a-zA-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý ]+$/;
  if (regex.test(str)) {
    return true;
  }
  return null;
};

// export const validateOrgs = value => {
//   const str = value.split();
//   const rex = /^\w{3,250}$/;
//   return rex.test(str);
// }
export const validateRoleName = (
  value,
  rules = {
    minLength: 5,
    maxLength: 250,
  },
) => {
  const str = value || '';
  if (str.length > rules.maxLength)
    return `Độ dài giá trị nhập vào phải nhỏ hơn ${rules.maxLength}`;
  if (str.length < rules.minLength)
    return `Độ dài giá trị nhập vào phải lớn hơn ${rules.minLength}`;
  // const regex = /^[0-9a-z ]+$/;
  const regex = /^[a-z0-9A-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý ]+$/;
  if (!regex.test(str)) {
    return `Chỉ được phép nhập chữ cái, số`;
  }
  return null;
};
export const validateNameRole = (
  value,
  rules = {
    minLength: 0,
    maxLength: 50,
  },
) => {
  const str = value || '';
  if (str.length > rules.maxLength)
    return `Độ dài giá trị nhập vào phải nhỏ hơn ${rules.maxLength}`;
  if (str.length < rules.minLength)
    return `Độ dài giá trị nhập vào phải lớn hơn ${rules.minLength}`;
  // const regex = /^[0-9a-z ]+$/;
  const regex = /^[a-zA-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý ]+$/;
  if (!regex.test(str)) {
    return `Chỉ được phép nhập chữ cái, số`;
  }
  return null;
};

export const validatePrintConfig = (
  value,
  rules = {
    minLength: 0,
    maxLength: 250,
  },
) => {
  const str = value || '';
  if (str.length > rules.maxLength)
    return `Độ dài giá trị nhập vào phải nhỏ hơn ${rules.maxLength}`;
  if (str.length < rules.minLength)
    return `Độ dài giá trị nhập vào phải lớn hơn ${rules.minLength}`;
  // const regex = /^[0-9a-z ]+$/;
  const regex = /^[a-z0-9A-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý ]+$/;
  if (!regex.test(str)) {
    return `Chỉ được phép nhập chữ cái, số`;
  }
  return null;
};

// export const validateFullName = value => {
//   const str = value.split(' ');
//   const regex = /(\b([A-Z]{1})[\Sa-zÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý])/;
//   for (let i = 0; i < str.length; i += 1) {
//     if (!regex.test(str[i])) {
//       return `Chỉ được phép nhập chữ cái, số`;
//     }
//   }
//   return null;
// };

export const validateFullName = (
  value,
  rules = {
    minLength: 5,
    maxLength: 50,
  },
) => {
  const str = value || '';
  if (str.length > rules.maxLength) return false;
  if (str.length < rules.minLength) return false;
  const regex = /^[a-zA-Z_/\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý ]+$/;
  const regex2 = /^(?=.*[A-Z])(?=.*[a-z])/;
  if (regex.test(str) && regex2.test(str)) {
    return true;
  }
  return null;
};
