/* eslint-disable prefer-destructuring */
/* eslint-disable no-plusplus */
/* eslint-disable no-bitwise */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import moment from 'moment';
import { put } from 'redux-saga/effects';

import {
  DATE_FORMAT,
  REQUEST_METHOD,
  STATUS_CODE,
  TEMPLATE_URL,
  LIST_ITEM_URL,
  LIST_ITEM_TYPE,
  WORKFLOW_CODE,
  ALL_STORAGE_PAGES,
} from './constants';
import request from './request';
import { sampleContents } from './commonConfig';
import { changeLoginStatus } from '../containers/App/actions';
import { clearStorage } from './storage';

export const convertToFormData = data => {
  const formData = new FormData();
  for (const key in data) {
    formData.append(key, data[key]);
  }
  return formData;
};

export const convertToDateString = (date, format = DATE_FORMAT.DATE_TIME) =>
  moment(date).format(format);
export const convertToDateString2 = (date, format = DATE_FORMAT.DATE) =>
  moment(date).format(format);

export const formatNumber = number =>
  number.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');

export const getCountByWorkflowType = (statuses, codes) => {
  // console.log('statuses', statuses);
  // console.log('codes', codes);

  if (!statuses || statuses.length === 0 || !codes || codes.length === 0)
    return 0;
  let count = 0;

  Object.keys(codes).forEach(key => {
    const item = statuses.find(status => codes[key] === status.label);

    count += item ? item.count : 0;
  });

  return count;
};

export const formatBirthDate = date => {
  if (date) {
    const output = [];
    const sNumber = date.toString();
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < sNumber.length; i++) {
      output.push(+sNumber.charAt(i));
    }
    if (output.length === 8) {
      return `${output[0]}${output[1]}/${output[2]}${output[3]}/${output[4]}${output[5]
        }${output[6]}${output[7]}`;
    }
    if (output.length === 6) {
      return `${output[0]}${output[1]}/${output[2]}${output[3]}${output[4]}${output[5]
        }`;
    }
    if (output.length === 4) {
      return `${output[0]}${output[1]}${output[2]}${output[3]}`;
    }
  }
  return date;
};

// export const bloodGroupName = code => {
//   // eslint-disable-next-line radix
//   if (!code || parseInt(code) > 4) {
//     // eslint-disable-next-line no-param-reassign
//     code = 0;
//   }
//   // eslint-disable-next-line radix
//   const data = groupBlood.find(obj => obj.code === parseInt(code));
//   return data.name;
// };

// format file size
export const convertFileSize = bytes => {
  if (bytes <= 0) {
    return '0 B';
  }
  const ratio = 1024;
  const sizes = ['B', 'KB', 'MB', 'GB'];
  const i = Math.floor(Math.log(bytes) / Math.log(ratio));

  return `${parseFloat((bytes / Math.pow(ratio, i)).toFixed(2))} ${sizes[i]}`;
};

export const serializeQuery = query =>
  query
    ? Object.keys(query)
      .map(key => `${key}=${query[key]}`)
      .join('&')
    : '';

export const s2ab = string => {
  const buffer = new ArrayBuffer(string.length);
  const view = new Uint8Array(buffer);
  for (let i = 0; i < string.length; i += 1) {
    view[i] = string.charCodeAt(i) & 0xff;
  }
  return buffer;
};

export const isInRole = (userRoles, routeRoles) => {
  const role = routeRoles.find(
    routeRole => userRoles.find(userRole => routeRole === userRole) != null,
  );
  console.log('role');
  console.log(role);
  return role != null;
};

export const getSampleDisplayName = sampleCode => {
  let displayName = '';

  for (let i = 0; i < sampleContents.length; i++) {
    if (sampleContents[i].code === sampleCode) {
      displayName = sampleContents[i].displayName;
      break;
    }
  }

  return displayName;
};

export const getSampleContents = async sampleCode => {
  const [, workflowCode, taskCode, responseCode] = sampleCode.split('.');
  const body = {
    filter: {
      workflowCode: [workflowCode],
      taskCode: [taskCode],
      responseCode: [responseCode],
    },
  };

  const url = `${TEMPLATE_URL}/query`;
  const response = await request(url, {
    method: REQUEST_METHOD.POST,
    body: JSON.stringify(body),
  });

  if (response.status === STATUS_CODE.SUCCESS) {
    response.results = response.results.map((r, i) => ({
      ...r,
      code: sampleCode,
      displayName: getSampleDisplayName(sampleCode),
      comment: r.templateResponse,
      order: i + 1,
    }));
    return response.results;
  }
  return [];
};

export const getWorkflowIOFields = async code => {
  const url = `${LIST_ITEM_URL}/query`;
  try {
    const bizByCodeBody = { itemType: LIST_ITEM_TYPE.BIS_SERVICE, code };

    const bizByCodeResponse = await request(url, {
      method: REQUEST_METHOD.POST,
      body: JSON.stringify(bizByCodeBody),
    });

    if (bizByCodeResponse && bizByCodeResponse.status === STATUS_CODE.SUCCESS) {
      const { categories } = bizByCodeResponse;
      if (categories && categories[0]) {
        const data = { id: categories[0].id };

        const getInputFields = request(url, {
          method: REQUEST_METHOD.POST,
          body: JSON.stringify({
            itemType: LIST_ITEM_TYPE.FILTER_FIELD,
            parentId: data.id,
          }),
        });

        const getOutputFields = request(url, {
          method: REQUEST_METHOD.POST,
          body: JSON.stringify({
            itemType: LIST_ITEM_TYPE.OUTPUT_FIELD,
            parentId: data.id,
          }),
        });
        const responses = await Promise.all([getInputFields, getOutputFields]);

        return {
          inputFields: responses[0].categories,
          outputFields: responses[1].categories,
        };
      }
    }
  } catch (error) {
    console.log('error', error);
  }
  return {};
};

export const applyConfigWorkflowCodeToQuery = query => {
  const tmp = { ...query };
  tmp.filter = { ...tmp.filter };
  tmp.filter.workflowCode = [WORKFLOW_CODE.INTERGRATION];

  return tmp;
};

export const parseListFieldWithChild = array => {
  const tmp = [...array];
  return tmp
    .map(item => {
      const extraData = JSON.parse(item.extraDataJson);
      const result = { ...item, ...extraData, hasChild: false };
      if (!item.parentId) {
        result.hasChild =
          array.findIndex(innerItem => innerItem.parentId === item.id) > -1;
      }
      return result;
    })
    .sort((a, b) => a.listOrder - b.listOrder);
};

export const formatDayInChart = (value, type) => {
  const values = value.split('');

  if (type === 'day') {
    return `${values[6]}${values[7]}/${values[4]}${values[5]}`;
  }
  if (type === 'week') {
    return `${values[4]}${values[5]}`;
  }
  if (type === 'month') {
    return `${values[4]}${values[5]}`;
  }
  if (type === 'quarter') {
    return `${values[4]}`;
  }

  return value;
};

export const getWeekInYear = () => {
  const year = new Date().getFullYear();
  const weekDay = moment([year, 0, 1]).isoWeekday();
  const currentDayInYear = moment().dayOfYear();
  if (weekDay === 1) {
    const week = Math.ceil(currentDayInYear / 7);
    return week;
  }
  const week = Math.ceil((currentDayInYear - weekDay) / 7 + 1);

  return week;
  // const currentWeekInYear =
};
export const saveQuery = (page, query) => {
  try {
    localStorage.setItem(page, JSON.stringify(query));
  } catch (error) {
    localStorage.removeItem(page);
  }
};
export const getQuery = key => {
  try {
    return localStorage.getItem(key);
  } catch (error) {
    localStorage.removeItem(key);
  }
};

export const getQueryLocalStorage = page => {
  try {
    ALL_STORAGE_PAGES.forEach(pageName => {
      if (pageName !== page) {
        localStorage.removeItem(pageName);
      }
    });
    if (!page) return null;
    const queryStr = localStorage.getItem(page);
    return JSON.parse(queryStr);
  } catch (error) {
    return null;
  }
};
export const clearQueryLocalStorage = () => {
  getQueryLocalStorage();
};

export function* checkToken(error) {
  if (error.status === 403) {
    clearStorage();
    yield put(changeLoginStatus(false));
  }
}
