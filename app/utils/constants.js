export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';


export const PUBLIC_PATH = 'efast';

export const APP_URL =
    process.env.NODE_ENV === 'development'
        ? 'http://localhost:8092'
        : '/syncdata';

// LOGIN API URL
export const LOGIN_URL = `${APP_URL}/api/v1/login`;
export const TEMPLATE_URL = `${APP_URL}/common/api/v1/template-responses`;

export const REQUEST_METHOD = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
    PATCH: 'PATCH',
};

export const ROLES = {
    MAKER: 'MAKER',
    APPROVER: 'APPROVER',
};

export const FUNCTION_ROLES = {
    BHXH: 'BHXH',
    TTHD: 'TTHD',
};

export const STATUS_CODE = {
    SUCCESS: '1',
};

export const FORMAT_TYPE = {
    DATE: 'date',
    BIRTH_DATE: 'birth_date',
    DATE_TIME: 'datetime',
    TIME: 'time',
    PROGRESS: 'PROGRESS',
    NUMBER: 'number',
    LINK: 'link',
    MONTH: 'month',
    FILE_SIZE: 'file_size',
    TOOLTIP: 'tooltip',
};

export const FREQUENCY = {
    DAILY: 'daily',
    WEEKLY: 'weekly',
    MONTHLY: 'monthly',
    QUARTERLY: 'quarterly',
    YEARLY: 'yearly',
};

export const PRINT_TEMPLATE_VALUE = {
    REPORT: 1,
    SEARCH: 2,
    REQUEST_APPROVAL: 3,
};

export const REPORT_TYPE = {
    COLLATION: 1,
    ERRFILE: 2,
    FILESIZE: 3,
};