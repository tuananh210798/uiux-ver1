import {
  FORMAT_TYPE,
  REPORT_TYPE,
  VIEW_CONFIG_PREFIX,
  FREQUENCY,
  PRINT_TEMPLATE_VALUE,
  // VIEW_CONFIG_USER,
  // VIEW_CONFIG_COLLATION,
  // VIEW_CONFIG_COLLATION_DASHBOARD,
  // VIEW_CONFIG_COLLATION_DETAIL,
  // VIEW_CONFIG_SEARCH,
  // VIEW_CONFIG_SEARCH_DETAIL,
  // VIEW_CONFIG_CONTENT,
} from './constants';

export const VIEW_CONFIG = {
  // dangtq
  SEARCH_LIST: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.SEARCH_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        width: 50,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'fileName',
        title: 'Tên file',
        checked: true,
        order: 2,
        formatType: FORMAT_TYPE.LINK,
        width: 300,
      },
      {
        name: 'totalRecordRS',
        title: 'Tổng số bản ghi/ hồ sơ gửi/ nhận',
        checked: true,
        order: 3,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'insertRecordRS',
        title: 'Bản ghi/ hồ sơ Insert gửi/ nhận',
        checked: true,
        order: 4,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'updateRecordRS',
        title: 'Bản ghi/ hồ sơ Update gửi/ nhận',
        checked: true,
        order: 5,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'deleteRecordRS',
        title: 'Bản ghi/ hồ sơ Delete gửi/ nhận',
        checked: true,
        order: 6,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 7,
        // formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'reconcileTime',
        title: 'Ngày/giờ nhận file',
        checked: true,
        order: 8,
        formatType: FORMAT_TYPE.DATE_TIME,
      },
      {
        name: 'fileSize',
        title: 'Dung lượng file (Byte)',
        checked: true,
        order: 9,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'dataReceiveStatus',
        title: 'Trạng thái',
        checked: true,
        order: 10,
        width: 100,
      },
    ],
  },
  SEARCH_LIST_SEND: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.SEARCH_SEND.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        width: 50,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'fileName',
        title: 'Tên file',
        checked: true,
        order: 2,
        formatType: FORMAT_TYPE.LINK,
        width: 350,
      },
      {
        name: 'totalRecord',
        title: 'Tổng số bản ghi',
        checked: true,
        order: 3,
        formatType: FORMAT_TYPE.NUMBER,
        // width: 180,
      },
      {
        name: 'insertRecord',
        title: 'Bản ghi Insert gửi',
        checked: true,
        order: 4,
        formatType: FORMAT_TYPE.NUMBER,
        // width: 180,
      },
      {
        name: 'updateRecord',
        title: 'Bản ghi Update gửi',
        checked: true,
        order: 5,
        formatType: FORMAT_TYPE.NUMBER,
        // width: 180,
      },
      {
        name: 'deleteRecord',
        title: 'Bản ghi Delete gửi',
        checked: true,
        order: 6,
        formatType: FORMAT_TYPE.NUMBER,
        // width: 180,
      },
      {
        name: 'appCode',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 7,
        // formatType: FORMAT_TYPE.NUMBER,
        width: 250,
      },
      {
        name: 'createdTime',
        title: 'Ngày/giờ gửi',
        checked: true,
        order: 8,
        formatType: FORMAT_TYPE.DATE_TIME,
        // width: 180,
      },
      {
        name: 'status',
        title: 'Trạng thái',
        checked: true,
        order: 9,
        formatType: FORMAT_TYPE.NUMBER,
        width: 100,
      },
    ],
  },
  SEARCH_DETAIL: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.SEARCH_DETAIL_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
        width: 150,
      },
      {
        name: 'collation_content',
        title: 'Nội dung',
        checked: true,
        order: 2,
      },
      {
        name: 'collation_detail',
        title: 'Chi tiết file nhận',
        checked: true,
        order: 3,
        // width: 180,
      },
    ],
  },
  COLLATION_LIST: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.COLLATION_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
        width: 50,
        isSortable: false,
      },
      {
        name: 'fileName',
        title: 'Tên file',
        checked: true,
        order: 2,
        formatType: FORMAT_TYPE.LINK,
        width: 300,
      },
      {
        name: 'totalRecordRS',
        title: 'Tổng số bản ghi/ hồ sơ gửi/ nhận',
        checked: true,
        order: 3,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
        // width: 180,
      },
      {
        name: 'insertRecordRS',
        title: 'Bản ghi/ hồ sơ Insert gửi/ nhận',
        checked: true,
        order: 4,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
        // width: 180,
      },
      {
        name: 'updateRecordRS',
        title: 'Bản ghi/ hồ sơ Update gửi/ nhận',
        checked: true,
        order: 5,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
        // width: 180,
      },
      {
        name: 'deleteRecordRS',
        title: 'Bản ghi/ hồ sơ Delete gửi/ nhận',
        checked: true,
        order: 6,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
        // width: 180,
      },
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 7,
        // formatType: FORMAT_TYPE.NUMBER,
        // width: 250,
      },
      {
        name: 'reconciledTime',
        title: 'Ngày/giờ đối chiếu',
        checked: true,
        formatType: FORMAT_TYPE.DATE_TIME,
        order: 8,
        // width: 180,
      },
      {
        name: 'fileSize',
        title: 'Dung lượng file (Byte)',
        checked: true,
        formatType: FORMAT_TYPE.NUMBER,
        order: 9,
        // width: 180,
      },
      {
        name: 'reconciledStatus',
        title: 'Trạng thái',
        checked: true,
        order: 10,
        width: 100,
      },
    ],
  },
  COLLATION_DASHBOARD: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.COLLATION_DASHBOARD.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
        width: 100,
      },
      {
        name: 'fileName',
        title: 'Tên file',
        checked: true,
        order: 2,
        // formatType: FORMAT_TYPE.LINK,
      },
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 3,
      },
      {
        name: 'reconciledStatus',
        title: 'Trạng thái',
        checked: true,
        order: 4,
      },
    ],
  },
  COLLATION_DETAIL: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.COLLATION_DETAIL_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
        width: 200,
      },
      {
        name: 'collation_content',
        title: 'Nội dung',
        checked: true,
        order: 2,
      },
      {
        name: 'collation_detail',
        title: 'Chi tiết đối chiếu',
        checked: true,
        order: 3,
        // width: 180,
      },
    ],
  },
  COLLATION_SEND_DETAIL: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.COLLATION_DETAIL_SEND.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'collation_content',
        title: 'Nội dung',
        checked: true,
        order: 2,
      },
      {
        name: 'collation_detail',
        title: 'Chi tiết file gửi',
        checked: true,
        order: 3,
        // width: 180,
      },
    ],
  },
  RISK_LIST: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.RISK_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        width: 50,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'fileName',
        title: 'Tên file',
        checked: true,
        order: 2,
        formatType: FORMAT_TYPE.LINK,
        // width: 180,
      },
      {
        name: 'totalRecord',
        title: 'Tổng số bản ghi',
        checked: true,
        order: 3,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'insertRecord',
        title: 'Bản ghi Insert',
        checked: true,
        order: 4,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'updateRecord',
        title: 'Bản ghi Update',
        checked: true,
        order: 5,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'deleteRecord',
        title: 'Bản ghi Delete',
        checked: true,
        order: 6,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 7,
        width: 250,
      },
      {
        name: 'createdTime',
        title: 'Ngày/giờ tiếp nhận',
        checked: true,
        order: 8,
        // width: 180,
        formatType: FORMAT_TYPE.DATE_TIME,
      },
      {
        name: 'riskContent',
        title: 'Nội dung cảnh báo',
        checked: true,
        order: 9,
        formatType: FORMAT_TYPE.NUMBER,
        width: 250,
      },
      {
        name: 'riskStatus',
        title: 'Trạng thái',
        checked: true,
        order: 10,
        formatType: FORMAT_TYPE.NUMBER,
        // width: 180,
      },
    ],
  },
  RISK_DASHBOARD: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.RISK_DASHBOARD.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        width: 100,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'fileName',
        title: 'Tên file',
        checked: true,
        order: 2,
        // formatType: FORMAT_TYPE.LINK,
        // width: 180,
      },
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 7,
        // width: 250,
      },
      {
        name: 'riskStatus',
        title: 'Trạng thái',
        checked: true,
        order: 10,
        formatType: FORMAT_TYPE.NUMBER,
        // width: 180,
      },
    ],
  },
  RISK_DETAIL: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.RISK_DETAIL_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
        width: 180,
      },
      {
        name: 'content',
        title: 'Nội dung',
        checked: true,
        order: 2,
        // width: 180,
      },
      {
        name: 'detailContent',
        title: 'Chi tiết cảnh báo rủi ro',
        checked: true,
        order: 3,
        // width: 180,
      },
    ],
  },
  USER_LIST: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.USER_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        formatType: FORMAT_TYPE.NUMBER,
        width: 50,
        isSortable: false,
      },
      // {
      //   name: 'userId',
      //   title: 'Mã ID',
      //   checked: true,
      //   order: 2,
      // },
      {
        name: 'fullName',
        title: 'Tên người dùng',
        checked: true,
        order: 3,
      },
      {
        name: 'username',
        title: 'Tên đăng nhập',
        checked: true,
        order: 4,
      },
      {
        name: 'email',
        title: 'Email',
        checked: true,
        order: 5,
      },
      {
        name: 'organization',
        title: 'Đơn vị',
        checked: true,
        order: 6,
      },
      {
        name: 'roleName',
        title: 'Vai trò',
        checked: true,
        order: 7,
      },
      {
        name: 'status',
        title: 'Trạng thái',
        checked: true,
        order: 8,
        formatType: FORMAT_TYPE.NUMBER,
        width: 100,
      },
      {
        name: 'actions',
        title: 'Thao tác',
        checked: true,
        order: 9,
        isRightColumn: true,
        isDragable: false,
        isSortable: false,
      },
    ],
  },
  REPORT_COLLATION: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.REPORT_COLLATION_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 1,
        // width: 150,
      },
      {
        name: 'totalRecordRS',
        title: 'Tổng số bản ghi gửi/ nhận',
        checked: true,
        order: 2,
        // width: 180,
      },
      {
        name: 'insertRecordRS',
        title: 'Bản ghi Insert gửi/ nhận',
        checked: true,
        order: 3,
        // width: 180,
      },
      {
        name: 'updateRecordRS',
        title: 'Bản ghi update gửi/ nhận',
        checked: true,
        order: 4,
        // width: 180,
      },
      {
        name: 'deleteRecordRS',
        title: 'Bản ghi delete gửi/ nhận',
        checked: true,
        order: 5,
        // width: 180,
      },
    ],
  },
  REPORT_ERROR: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.REPORT_ERROR_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'dataSource',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 1,
        // width: 150,
      },
      {
        name: 'totalErrorFile',
        title: 'Tổng số file lỗi',
        checked: true,
        order: 2,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'totalResentFile',
        title: 'Số lần gửi lại',
        checked: true,
        order: 3,
        formatType: FORMAT_TYPE.NUMBER,
      },
    ],
  },
  REPORT_RISK: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.REPORT_RISK_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'appCode',
        title: 'Nguồn dữ liệu',
        checked: true,
        order: 1,
        // width: 150,
      },
      {
        name: 'status',
        title: 'Trạng thái',
        checked: true,
        order: 2,
        // width: 180,F
      },
      {
        name: 'totalRecord',
        title: 'Tổng số bản ghi',
        checked: true,
        order: 3,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'insertRecord',
        title: 'Số bản ghi Insert',
        checked: true,
        order: 4,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'updateRecord',
        title: 'Số bản ghi Update',
        checked: true,
        order: 5,
        formatType: FORMAT_TYPE.NUMBER,
      },
      {
        name: 'deleteRecord',
        title: 'Số bản ghi Delete',
        checked: true,
        order: 5,
        formatType: FORMAT_TYPE.NUMBER,
      },
    ],
  },
  CONFIG_REPORT: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.CONFIG_REPORT.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        width: 180,
        formatType: 'number',
      },
      {
        name: 'code',
        title: 'Mã nguồn dữ liệu',
        checked: true,
        order: 2,
        width: 300,

      },
      {
        name: 'displayName',
        title: 'Tên nguồn dữ liệu',
        checked: true,
        order: 3,
      },
      {
        name: 'checkFile',
        title: 'Bản ghi',
        checked: true,
        order: 4,
        width: 250,
        formatType: 'number'
      },
      {
        name: 'checkRecord',
        title: 'Hồ sơ',
        checked: true,
        order: 5,
        width: 250,
        formatType: 'number'
      },
    ]
  },

  //
  PERMISSION_USER: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.PERMISSION_PAGE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        // width: 150,
      },
      {
        name: 'userName',
        title: 'Tên người dùng',
        checked: true,
        order: 2,
        // width: 180,
      },
      {
        name: '',
        title: 'Lãnh đạo',
        checked: true,
        order: 3,
        // width: 180,
      },
      {
        name: '',
        title: 'Cán bộ',
        checked: true,
        order: 4,
        // width: 180,
      },
      {
        name: '',
        title: 'Chuyên viên',
        checked: true,
        order: 5,
        // width: 180,
      },
      {
        name: '',
        title: 'Quản trị',
        checked: true,
        order: 6,
        // width: 180,
      },
    ],
  },
  CONFIG_ROLE: {
    PARAMETER: `${VIEW_CONFIG_PREFIX}.CONFIG_ROLE.LIST_TABLE`,
    DEFAULT_COLUMNS: [
      {
        name: 'order',
        title: 'STT',
        checked: true,
        order: 1,
        width: 50,
        formatType: FORMAT_TYPE.NUMBER,
        isSortable: false,
      },
      {
        name: 'roleCode',
        title: 'Mã vai trò',
        checked: true,
        order: 2,
        isSortable: false,
      },
      {
        name: 'name',
        title: 'Tên vai trò',
        checked: true,
        order: 3,
        isSortable: false,
      },
      {
        name: 'actions',
        title: 'Thao tác',
        checked: true,
        order: 4,
        isRightColumn: true,
        isDragable: false,
        isSortable: false,
        width: 200,
      },
    ],
  },
};

export const LIST_MONTH = [
  {
    label: '1',
    value: 1,
  },
  {
    label: '2',
    value: 2,
  },
  {
    label: '3',
    value: 3,
  },
  {
    label: '4',
    value: 4,
  },
  {
    label: '5',
    value: 5,
  },
  {
    label: '6',
    value: 6,
  },
  {
    label: '7',
    value: 7,
  },
  {
    label: '8',
    value: 8,
  },
  {
    label: '9',
    value: 9,
  },
  {
    label: '10',
    value: 10,
  },
  {
    label: '11',
    value: 11,
  },
  {
    label: '12',
    value: 12,
  },
];
export const QUARTER = [
  {
    label: '1',
    value: 1,
  },
  {
    label: '2',
    value: 2,
  },
  {
    label: '3',
    value: 3,
  },
  {
    label: '4',
    value: 4,
  },
];

export const reportFrequencies = [
  {
    value: 1,
    displayName: 'Tuần',
  },
  {
    value: 2,
    displayName: 'Tháng',
  },
  {
    value: 3,
    displayName: 'Quý',
  },
  {
    value: 5,
    displayName: '6 Tháng đầu năm',
  },
  {
    value: 4,
    displayName: 'Năm',
  },
];

export const scheduleFrequencies = [
  {
    value: FREQUENCY.DAILY,
    displayName: 'Ngày',
  },
  {
    value: FREQUENCY.WEEKLY,
    displayName: 'Tuần',
  },
  {
    value: FREQUENCY.MONTHLY,
    displayName: 'Tháng',
  },
  {
    value: FREQUENCY.QUARTERLY,
    displayName: 'Quý',
  },
  {
    value: FREQUENCY.YEARLY,
    displayName: 'Năm',
  },
];

export const printTemplates = [
  {
    label: 'Báo cáo',
    value: PRINT_TEMPLATE_VALUE.REPORT,
  },
  // {
  //   label: 'Tra cứu',
  //   value: PRINT_TEMPLATE_VALUE.SEARCH,
  // },
  // {
  //   label: 'Gửi yêu cầu phê duyệt',
  //   value: PRINT_TEMPLATE_VALUE.REQUEST_APPROVAL,
  // },
];
export const reportTypes = [
  {
    label: 'Báo cáo theo kết quả đối chiếu',
    value: REPORT_TYPE.COLLATION,
  },
  {
    label: 'Báo cáo theo số lượng file lỗi',
    value: REPORT_TYPE.ERRFILE,
  },
  {
    label: 'Báo cáo theo dung lượng File',
    value: REPORT_TYPE.FILESIZE,
  },
];
export const reconciledStatus = [
  {
    value: '1',
    label: 'Đủ dữ liệu',
  },
  {
    value: '2',
    label: 'Sai dữ liệu',
  },
];
