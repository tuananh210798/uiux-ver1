export const checkSignIn = () => {
  const accessToken = localStorage.getItem('accessToken');
  if (!accessToken || !accessToken.length) {
    return !!sessionStorage.getItem('accessToken');
  }
  return true;
};

export const getStorage = key => {
  const data = localStorage.getItem(key);
  if (!data || data.length > 0) {
    return sessionStorage.getItem(key);
  }
  return data;
};

export const getAccessToken = () => {
  const accessToken = localStorage.getItem('accessToken');
  if (!accessToken || !accessToken.length) {
    return sessionStorage.getItem('accessToken');
  }
  return accessToken;
};

export const clearStorage = () => {
  localStorage.clear();
  sessionStorage.clear();
};
