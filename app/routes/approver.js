import HomePage from './../containers/HomePage/Loadable';
import { FormattedMessage, } from 'react-intl';
import AccountAccountList from './../containers/AccountAccountList/Loadable';
import BulkTransfer from './../containers/BulkTransfer/Loadable';
import { AccessDenied } from './../containers/AccessDenied/index';

const approverRoutes = [
    {
        path: '/home',
        name: 'Dashboard',
        icon: '',
        component: AccountAccountList,
    },
    {
        path: '/account',
        name: 'routes.maker.account',
        icon: '',
        component: AccountAccountList,
        exact: true,
        roles: ['uix']
    },
    {
        path: '/transfer',
        name: 'routes.maker.transfer',
        icon: '',
        component: BulkTransfer,
        exact: true,
    },
    {
        name: "routes.maker.transfer",
        icon: '',
        collapse: true,
        views: [
            {
                path: '/listUser',
                type: 'source',
                name: 'Quản lý người dùng',
                component: BulkTransfer,
                mini: 'US',
            },
            {
                path: '/configRole',
                type: 'source',
                name: 'Quản lý vai trò',
                component: BulkTransfer,
                mini: 'CR',
            },
            {
                path: '/configPermiss',
                type: 'source',
                name: 'Thiết lập phân quyền',
                component: BulkTransfer,
                mini: 'CP',
                //hide: true,
            },
        ]
    },
    // {
    //     path: '/receiveFile',
    //     name: 'Tra cứu',
    //     icon: Search,
    //     component: ReceiveFilePage,
    //     exact: true,
    // },
    // {
    //     path: '/receiveFile/:id',
    //     name: 'Tra cứu chi tiết',
    //     component: ReceiveFileDetailPage,
    //     hide: true,
    // },
    // {
    //     collapse: true,
    //     name: 'Cấu Hình',
    //     state: 'openSetting',
    //     icon: Settings,
    //     views: [
    //         {
    //             path: '/setting/print',
    //             name: 'Cấu hình trang in',
    //             component: PrintConfigPage,
    //             mini: 'CW',
    //             //roles: [ROLES.ADMIN],
    //         },
    //         {
    //             path: '/setting/configReport',
    //             name: 'Cấu hình hiển thị',
    //             component: ConfigReport,
    //             mini: 'CR',
    //             //roles: [ROLES.ADMIN],
    //         },
    //     ],
    // },
    // {
    //     collapse: true,
    //     name: 'Báo cáo',
    //     icon: Assessment,
    //     state: 'openReport',
    //     views: [
    //         {
    //             path: '/report/collation',
    //             type: 'source',
    //             name: 'Theo kết quả đối chiếu',
    //             mini: 'RC',
    //             component: ReportPage,
    //             // roles: [ROLES.ADMIN],
    //         },
    //         {
    //             path: '/report/fileErr',
    //             type: 'receiver',
    //             name: 'Theo số lượng file lỗi',
    //             mini: 'FE',
    //             component: ReportPage,
    //         },
    //         {
    //             path: '/report/fileSize',
    //             type: 'receiver',
    //             name: 'Theo dung lượng file',
    //             mini: 'FS',
    //             component: ReportPage,
    //         },
    //     ],
    // },
    // {
    //     path: '/document',
    //     name: 'Hướng dẫn sử dụng',
    //     icon: FileCopy,
    //     component: DocumentPage,
    //     exact: true,
    // },
    // {
    //     redirect: true,
    //     path: '/',
    //     pathTo: '/internalDashboard',
    //     name: 'Dashboard',
    //     exact: true,
    // },
];

export default approverRoutes;