import { Fingerprint } from '@material-ui/icons';
import LoginPage from '../containers/LoginPage';

// material-ui-icons

// const Demo = () => <div>Ho</div>;
const pagesRoutes = [
    {
        path: '/login',
        name: 'Đăng nhập',
        short: 'Đăng nhập',
        mini: 'LP',
        icon: Fingerprint,
        component: LoginPage,
    },
];

export default pagesRoutes;
