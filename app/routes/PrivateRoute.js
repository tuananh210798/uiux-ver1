import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getAccessToken } from './../utils/storage';
import { isInRole } from './../utils/common';
import { difference, isEmpty } from 'lodash';

function checkRole(userRoles, roles) {
    let hasNoRole = false;
    if (!isEmpty(roles) && difference(roles, userRoles).length > 0) {
        return true;
    } else {
        return hasNoRole;
    }

}

export const PrivateRoute = ({ component: Component, roles, roleId, ...rest }) => (
    //...rest is path of route
    <Route {...rest} render={props => {
        // let currentUser = localStorage.getItem('user');
        // currentUser = JSON.parse(currentUser);
        // if(!currentUser){
        //     // not logged in so redirect to login page with the return url
        //     return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        // }

        let accessToken = getAccessToken();
        accessToken = JSON.parse(accessToken);
        // check if route is restricted by role
        if (roles) {
            console.log(checkRole(accessToken.roles, roles));
            if (checkRole(accessToken.roles, roles)) {
                return <Redirect to={{ pathname: '/access-denied' }} />
            }
        }

        //check maker or approver
        // if (roleId && currentUser.roleId !== roleId) {
        //     console.log(currentUser.roleId !== roleId);
        //     return <Redirect to={{ pathname: '/access-denied' }} />
        // }
        // localStorage.getItem('user')
        //     ? <Component {...props} />
        //     : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />

        return <Component {...props} />
    }} />
)