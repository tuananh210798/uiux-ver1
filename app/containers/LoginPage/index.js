/**
 *
 * LoginPage
 *
 */

import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect, useDispatch, useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLoginPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  Grid,
  Paper,
  IconButton,
  CssBaseline,
  Button,
  Checkbox,
  FormControlLabel,
  TextField,
  Box,
  Typography,
  Container
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Link from '@material-ui/core/Link';
import { VisibilityOutlined, VisibilityOffOutlined, LockOutlinedIcon } from '@material-ui/icons';
// import { makeStyles } from '@material-ui/core/styles';
import { loginData } from './actions';
import { validatePassword, validateUserNameLogin } from '../../utils/validation';
import CustomTextField from '../../components/CustomTextField';
import { clearQueryLocalStorage } from '../../utils/common';
import { Avatar } from '@material-ui/core/Avatar';

function Copyright() {

  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function LoginPage() {
  useInjectReducer({ key: 'loginPage', reducer });
  useInjectSaga({ key: 'loginPage', saga });

  const dispatch = useDispatch();
  const loginPage = useSelector(makeSelectLoginPage());
  const onLoginData = (data) => dispatch(loginData(data));
  const { loginSuccess } = loginPage;
  const [eyeState, setEyeState] = useState({
    eye1: false,
  });

  const [submitted, setSubmitted] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [remember, setRemenber] = useState(false);
  const [waring, setWarning] = useState('');
  const [textWarningUserName, setTextWarningUserName] = useState('');
  const [textWarningPassword, setTextWarningPassword] = useState('');
  const [focused, setFocused] = useState(false);

  // useEffect(
  //   () => () => {
  //     cleanup();
  //   },
  //   [],
  // );

  const updateEyeState = name => {
    const newEyeState = {
      ...eyeState,
      [name]: !eyeState[name],
    };

    setEyeState(newEyeState);
  };

  const endIcon = name => (
    <IconButton
      onClick={() => {
        updateEyeState(name);
      }}
    >
      {eyeState[name] ? <VisibilityOffOutlined /> : <VisibilityOutlined />}
    </IconButton>
  );

  const handleChecked = () => {
    if (remember === false) {
      setRemenber(true);
    } else {
      setRemenber(false);
    }
  };

  const handleLogin = useCallback(() => {
    if (validateUserNameLogin(username)) {
      setTextWarningUserName(
        'Nhập từ 5 kí tự đến 20 kí tự bao gồm chữ thường, số hoặc cả chữ và số',
      );
      setFocused(true);
    } else {
      setTextWarningUserName('');
    }

    if (!validatePassword(password)) {
      setTextWarningPassword(
        'Nhập từ 8 kí tự đến 32 kí tự bao gồm chữ hoa, chữ thường, số và kí tự đặc biệt',
      );
      setFocused(true);
    } else {
      setTextWarningPassword('');
    }

    const data = {
      remember,
      username,
      password,
    };
    setSubmitted(true);
    onLoginData(data);
  }, [username, password, remember]);

  useEffect(() => {
    if (!validateUserNameLogin(username)) {
      setTextWarningUserName('');
    }
    if (validatePassword(password)) {
      setTextWarningPassword('');
    }
  }, [username, password]);

  useEffect(() => {
    clearQueryLocalStorage();
    if (
      !validateUserNameLogin(username) &&
      validatePassword(password) &&
      loginSuccess === false
    ) {
      setWarning('Sai tài khoản hoặc mật khẩu');
    } else {
      setWarning('');
    }
  }, [loginSuccess]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Paper onKeyPress={e => { if (e.charCode === 13) { handleLogin(); } }}>
        {/* <Avatar> */}
        {/* <LockOutlinedIcon /> */}
        {/* </Avatar> */}
        <Typography component="h1" variant="h5">
          Sign in
            </Typography>
        {/* <form className="form" noValidate onSubmit={handleLogin}> */}
        <CustomTextField
          label="Username"
          type="text"
          name="username"
          value={username} onChange={e => setUsername(e.target.value)}
          error={validateUserNameLogin(username)}
          helperText={textWarningUserName}
          focused={focused}
        />
        {submitted && !username &&
          <Alert severity="error">Username is required</Alert>
        }
        <CustomTextField
          label="Password"
          type="password"
          name="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          type={eyeState.eye1 ? 'text' : 'password'}
          endIcon={endIcon('eye1')}
          error={!validatePassword(password)}
          helperText={textWarningPassword}
          focused={focused}
        />
        {submitted && !password &&
          <Alert severity="error">Password is required</Alert>
        }
        <Typography>
          {waring}
        </Typography>
        <FormControlLabel
          control={<Checkbox value="remember" color="primary" />}
          label="Remember me"
          onClick={handleChecked}
        />
        <Button
          // type="submit"
          fullWidth
          variant="contained"
          color="primary"
          // className="submit"
          onClick={handleLogin}
        >
          Sign In
            </Button>
        <Grid container>
          <Grid item xs>
            <Link href="#" variant="body2">
              Forgot password?
                </Link>
          </Grid>
          <Grid item>
            <Link href="#" variant="body2">
              {"Don't have an account? Sign Up"}
            </Link>
          </Grid>
        </Grid>
        {/* </form> */}
      </Paper>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

// LoginPage.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// };

// const mapStateToProps = createStructuredSelector({
//   loginPage: makeSelectLoginPage(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// );

// export default compose(withConnect)(LoginPage);
