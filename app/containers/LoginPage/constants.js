/*
 *
 * LoginPage constants
 *
 */

export const LOGIN_DATA = 'app/LoginPage/LOGIN_DATA';
export const LOGIN_DATA_SUCCESS = 'app/LoginPage/LOGIN_DATA_SUCCESS';
export const LOGIN_DATA_FAILURE = 'app/LoginPage/LOGIN_DATA_FAILURE';
export const CLEANUP = 'app/LoginPage/CLEANUP';
export const SHOW_SNACKBAR = 'app/AdminPage/SHOW_SNACKBAR';
