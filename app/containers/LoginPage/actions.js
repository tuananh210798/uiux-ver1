/*
 *
 * LoginPage actions
 *
 */

import {
  LOGIN_DATA,
  LOGIN_DATA_SUCCESS,
  LOGIN_DATA_FAILURE,
  SHOW_SNACKBAR,
} from './constants';


export function showSnackbar(data) {
  return {
    type: SHOW_SNACKBAR,
    data,
  };
}

export function loginData(data) {
  return {
    type: LOGIN_DATA,
    data,
  };
}
export function loginDataSuccess(data) {
  return {
    type: LOGIN_DATA_SUCCESS,
    data,
  };
}
export function loginDataFail(err) {
  return {
    type: LOGIN_DATA_FAILURE,
    err,
  };
}
