import { call, put, takeLatest } from 'redux-saga/effects';
import { loginDataSuccess, loginDataFail, showSnackbar } from './actions';
import { LOGIN_DATA } from './constants';
import { LOGIN_URL, PUBLIC_PATH, REQUEST_METHOD, STATUS_CODE } from '../../utils/constants';
import request from '../../utils/request';
import { changeLoginStatus, changeUserRole } from '../App/actions';
import { push } from 'connected-react-router';

// export function* loginDataSaga(action) {
//   const { data } = action;
//   const url = `${LOGIN_URL}`;
//   try {
//     const response = yield call(request, url, {
//       method: REQUEST_METHOD.POST,
//       body: JSON.stringify(data),
//     });
//     if (response.status === STATUS_CODE.SUCCESS) {
//       yield put(loginDataSuccess(response));
//       // console.log(localStorage.getItem('accessToken'));
//       if (data.remember) {
//         localStorage.setItem('accessToken', response.accessToken);
//         // console.log(localStorage.getItem('accessToken'));
//       } else {
//         sessionStorage.setItem('accessToken', response.accessToken);
//       }
//       yield put(changeLoginStatus(true));
//       yield put(
//         showSnackbar({
//           content: 'Đăng nhập thành công',
//           variant: 'success',
//         }),
//       );
//     } else {
//       yield put(
//         showSnackbar({
//           content: 'Sai tài khoản hoặc mật khẩu',
//           variant: 'error',
//         }),
//       );
//       yield put(loginDataFail());
//     }
//   } catch (error) {
//     yield put(showSnackbar({ content: error.message, variant: 'error' }));
//     yield put(loginDataFail(error));
//   }
// }

export function* loginDataSaga(action) {
  const { data } = action;
  const url = `${LOGIN_URL}`;
  try {
    // const response = yield call(request, url, {
    //   method: REQUEST_METHOD.POST,
    //   body: JSON.stringify(data),
    // });
    if (data.username === 'quy12345' && data.password === 'Quy12345@') { //maker
      data.roles = ['tthd', 'bhxh'];
      data.roleId = 'maker';
      yield put(loginDataSuccess(data));
      // console.log(localStorage.getItem('accessToken'));
      if (data.remember) {
        localStorage.setItem('accessToken', JSON.stringify(data));
        // console.log(localStorage.getItem('accessToken'));
      } else {
        sessionStorage.setItem('accessToken', JSON.stringify(data));
      }
      yield put(changeLoginStatus(true));
      yield put(changeUserRole('maker'));
      yield put(
        showSnackbar({
          content: 'Đăng nhập thành công',
          variant: 'success',
        }),
      );
      yield put(push('/' + PUBLIC_PATH));
    } else if (data.username === 'bach12345' && data.password === 'Quy12345@') {//approve
      data.roles = ['tthd', 'bhxh'];
      data.roleId = 'approver';
      yield put(loginDataSuccess(data));
      // console.log(localStorage.getItem('accessToken'));
      if (data.remember) {
        localStorage.setItem('accessToken', JSON.stringify(data));
        // console.log(localStorage.getItem('accessToken'));
      } else {
        sessionStorage.setItem('accessToken', JSON.stringify(data));
      }
      yield put(changeLoginStatus(true));
      yield put(changeUserRole('approver'));
      yield put(
        showSnackbar({
          content: 'Đăng nhập thành công',
          variant: 'success',
        }),
      );
      yield put(push('/' + PUBLIC_PATH));
    } else {
      yield put(
        showSnackbar({
          content: 'Sai tài khoản hoặc mật khẩu',
          variant: 'error',
        }),
      );
      yield put(loginDataFail());
    }
  } catch (error) {
    yield put(showSnackbar({ content: error.message, variant: 'error' }));
    yield put(loginDataFail(error));
  }
}

// Individual exports for testing
export default function* loginPageSaga() {
  yield takeLatest(LOGIN_DATA, loginDataSaga);
}
