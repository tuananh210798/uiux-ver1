/*
 *
 * LoginPage reducer
 *
 */
import produce from 'immer';
import {
  LOGIN_DATA,
  LOGIN_DATA_FAILURE,
  LOGIN_DATA_SUCCESS,
  CLEANUP,
} from './constants';
export const initialState = {
  loginSuccess: null,
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const loginPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOGIN_DATA:
        draft.loginSuccess = null;
        draft.isLoading = true;
        break;

      case LOGIN_DATA_SUCCESS:
        draft.loginSuccess = true;
        draft.isLoading = false;
        break;

      case LOGIN_DATA_FAILURE:
        draft.loginSuccess = false;
        draft.isLoading = false;
        break;
      case CLEANUP:
        draft.loginSuccess = null;
        draft.isLoading = false;
        break;
    }
  });

export default loginPageReducer;
