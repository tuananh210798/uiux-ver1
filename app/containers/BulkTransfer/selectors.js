import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the bulkTransfer state domain
 */

const selectBulkTransferDomain = state => state.bulkTransfer || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BulkTransfer
 */

const makeSelectBulkTransfer = () =>
  createSelector(
    selectBulkTransferDomain,
    substate => substate,
  );

export default makeSelectBulkTransfer;
export { selectBulkTransferDomain };
