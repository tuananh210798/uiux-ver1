/**
 *
 * Asynchronously loads the component for BulkTransfer
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
