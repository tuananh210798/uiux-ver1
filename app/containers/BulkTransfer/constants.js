/*
 *
 * BulkTransfer constants
 *
 */

export const DEFAULT_ACTION = 'app/BulkTransfer/DEFAULT_ACTION';
export const BULK_TRANSFER_CHANGE_ROUTE = 'app/BulkTransfer/BULK_TRANSFER_CHANGE_ROUTE';
