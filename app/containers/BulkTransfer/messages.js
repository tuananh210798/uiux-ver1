/*
 * BulkTransfer Messages
 *
 * This contains all the text for the BulkTransfer container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.BulkTransfer';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the BulkTransfer container!',
  },
});
