/*
 *
 * BulkTransfer actions
 *
 */

import { BULK_TRANSFER_CHANGE_ROUTE, DEFAULT_ACTION } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changeRoute() {
  return {
    type: BULK_TRANSFER_CHANGE_ROUTE,
  };
}
