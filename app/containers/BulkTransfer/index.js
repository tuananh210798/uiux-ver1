/**
 *
 * BulkTransfer
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectBulkTransfer from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { changeRoute } from './actions';

export function BulkTransfer() {
  useInjectReducer({ key: 'bulkTransfer', reducer });
  useInjectSaga({ key: 'bulkTransfer', saga });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changeRoute());
  }, []);

  return (
    <div>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

// BulkTransfer.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// };

// const mapStateToProps = createStructuredSelector({
//   bulkTransfer: makeSelectBulkTransfer(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// );

// export default compose(
//   withConnect,
//   memo,
// )(BulkTransfer);

export default memo(BulkTransfer);
