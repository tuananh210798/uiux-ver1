/*
 *
 * BulkTransfer reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, BULK_TRANSFER_CHANGE_ROUTE } from './constants';

export const initialState = {};

/* eslint-disable default-case, no-param-reassign */
const bulkTransferReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case DEFAULT_ACTION:
      case BULK_TRANSFER_CHANGE_ROUTE:
        break;
    }
  });

export default bulkTransferReducer;
