import { push } from 'connected-react-router';
import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { PUBLIC_PATH } from '../../utils/constants';
import { BULK_TRANSFER_CHANGE_ROUTE } from './constants';

function* changeRoute() {
  yield put(push('/' + PUBLIC_PATH + '/transfer'));
}

// Individual exports for testing
export default function* bulkTransferSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(BULK_TRANSFER_CHANGE_ROUTE, changeRoute);
}
