/**
 *
 * Asynchronously loads the component for Maker
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
