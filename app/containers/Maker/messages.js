/*
 * Maker Messages
 *
 * This contains all the text for the Maker container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Maker';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Maker container!',
  },
});
