import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the maker state domain
 */

const selectMakerDomain = state => state.maker || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Maker
 */

const makeSelectMaker = () =>
  createSelector(
    selectMakerDomain,
    substate => substate,
  );

export default makeSelectMaker;
export { selectMakerDomain };
