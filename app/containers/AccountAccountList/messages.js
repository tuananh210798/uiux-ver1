/*
 * AccountAccountList Messages
 *
 * This contains all the text for the AccountAccountList container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AccountAccountList';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AccountAccountList container 123!',
  },
});
