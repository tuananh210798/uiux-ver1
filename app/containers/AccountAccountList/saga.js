import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { PUBLIC_PATH } from '../../utils/constants';
import { ACCOUNT_LIST_CHANGE_ROUTE } from './constants'

function* changeRoute() {
  yield put(push('/' + PUBLIC_PATH + '/account'));
}

// Individual exports for testing
export default function* accountAccountListSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(ACCOUNT_LIST_CHANGE_ROUTE, changeRoute);
}
