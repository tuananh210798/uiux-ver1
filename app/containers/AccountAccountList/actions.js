/*
 *
 * AccountAccountList actions
 *
 */

import { DEFAULT_ACTION, ACCOUNT_LIST_CHANGE_ROUTE } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changeRoute() {
  return {
    type: ACCOUNT_LIST_CHANGE_ROUTE,
  };
}
