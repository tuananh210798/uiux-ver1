/*
 *
 * AccountAccountList reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, ACCOUNT_LIST_CHANGE_ROUTE } from './constants';

export const initialState = {};

/* eslint-disable default-case, no-param-reassign */
const accountAccountListReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case DEFAULT_ACTION:
      case ACCOUNT_LIST_CHANGE_ROUTE:
        break;
    }
  });

export default accountAccountListReducer;
