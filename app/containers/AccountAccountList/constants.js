/*
 *
 * AccountAccountList constants
 *
 */

export const DEFAULT_ACTION = 'app/AccountAccountList/DEFAULT_ACTION';
export const ACCOUNT_LIST_CHANGE_ROUTE = 'app/AccountAccountList/ACCOUNT_LIST_CHANGE_ROUTE';
