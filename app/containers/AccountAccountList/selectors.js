import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the accountAccountList state domain
 */

const selectAccountAccountListDomain = state =>
  state.accountAccountList || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AccountAccountList
 */

const makeSelectAccountAccountList = () =>
  createSelector(
    selectAccountAccountListDomain,
    substate => substate,
  );

export default makeSelectAccountAccountList;
export { selectAccountAccountListDomain };
