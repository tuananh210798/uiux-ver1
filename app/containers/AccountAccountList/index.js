/**
 *
 * AccountAccountList
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect, useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAccountAccountList from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { changeRoute } from './actions';

export function AccountAccountList() {
  useInjectReducer({ key: 'accountAccountList', reducer });
  useInjectSaga({ key: 'accountAccountList', saga });
  const dispatch = useDispatch();
  const accountAccountList = useSelector(makeSelectAccountAccountList());

  useEffect(() => {
    dispatch(changeRoute());
  }, [])
  return (
    <div>
      <Helmet>
        <title>AccountAccountList</title>
        <meta name="description" content="Description of AccountAccountList" />
      </Helmet>
      <FormattedMessage {...messages.header} />

    </div>
  );
}

AccountAccountList.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

// const mapStateToProps = createStructuredSelector({
//   accountAccountList: makeSelectAccountAccountList(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// );

export default memo(AccountAccountList);
