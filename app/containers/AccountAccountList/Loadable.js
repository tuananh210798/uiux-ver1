/**
 *
 * Asynchronously loads the component for AccountAccountList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
