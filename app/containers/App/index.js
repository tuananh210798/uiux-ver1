/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import Maker from 'containers/Maker/Loadable';
import Approver from 'containers/Approver/Loadable';

import GlobalStyle from '../../global-styles';
import makeSelectAppPage from './selectors';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { checkSignIn, getAccessToken } from './../../utils/storage';
import { PUBLIC_PATH } from '../../utils/constants';
import { changeLoginStatus } from './actions';
import { AccessDenied } from './../AccessDenied';
import { isEqual } from 'lodash';
import { changeUserRole } from './actions';
import { isEmpty } from 'lodash';

const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
`;

export default function App() {
  useInjectReducer({ key: 'appPage', reducer });

  // 
  useEffect(() => {
    const isLoginIn = checkSignIn();
    const accessToken = JSON.parse(getAccessToken());
    onChangeLoginStatus(isLoginIn);
    if (!isEmpty(accessToken)) {
      onChangeUserRole(accessToken.roleId);
    }
  }, []);
  const dispatch = useDispatch();
  const app = useSelector(makeSelectAppPage());
  const onChangeLoginStatus = (status) => dispatch(changeLoginStatus(status));
  const onChangeUserRole = (roleId) => dispatch(changeUserRole(roleId));

  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - React.js Boilerplate"
        defaultTitle="React.js Boilerplate"
      >
        <meta name="description" content="A React.js Boilerplate application" />
      </Helmet>

      <BrowserRouter basename={PUBLIC_PATH}>
        <Switch>
          {app.isLoginIn ? (
            isEqual(app.roleId, 'maker') ?
              <Route path="/" component={Maker} />
              : (isEqual(app.roleId, 'approver') ? <Route path="/" component={Approver} /> : <Route exact path='/access-denied' component={AccessDenied} />)
          ) : (
              [
                <Route exact path="/login" component={LoginPage} />,
                <Redirect from="/" to="/login" />,
              ]
            )}
        </Switch>
      </BrowserRouter>
      <GlobalStyle />
    </AppWrapper>
  );
}
