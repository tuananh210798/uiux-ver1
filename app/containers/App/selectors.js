import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectRouter = state => state.router;

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

const selectAppPageDomain = state => state.appPage || initialState;

const makeSelectAppPage = () =>
  createSelector(
    selectAppPageDomain,
    substate => substate,
  );

export default makeSelectAppPage;
export { selectAppPageDomain, makeSelectLocation };
