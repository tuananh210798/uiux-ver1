/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */
import produce from 'immer';
import { CHANGE_LOGIN_STATUS, CHANGE_USER_ROLE } from './constants';
export const initialState = {
  isLoginIn: true,
  roleId: ''
};

/* eslint-disable default-case, no-param-reassign */
const appPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_LOGIN_STATUS:
        draft.isLoginIn = action.isLoginIn;
      case CHANGE_USER_ROLE:
        draft.roleId = action.roleId;
        break;
    }
  });

export default appPageReducer;
