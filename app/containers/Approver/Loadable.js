/**
 *
 * Asynchronously loads the component for Approver
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
