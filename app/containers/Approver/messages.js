/*
 * Approver Messages
 *
 * This contains all the text for the Approver container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Approver';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Approver container!',
  },
});
