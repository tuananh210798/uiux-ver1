/**
 *
 * Approver
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectApprover from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import Header from '../../components/Layout/Header';
import Footer from '../../components/Layout/Footer';
import approverRoutes from '../../routes/approver';
import { Route, Switch } from 'react-router-dom';
import SideBar from '../../components/Layout/SideBar';
import { PrivateRoute } from '../../routes/PrivateRoute';
import AccessDenied from './../AccessDenied';

const switchRoutes = routes => (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
      if (prop.collapse)
        return prop.views.map((view, viewKey) => (
          <PrivateRoute
            path={view.path}
            key={viewKey}
            component={view.component}
            exact={view.exact}
            roles={view.roles ? view.roles : null}
          />
        ));
      return (
        <PrivateRoute
          path={prop.path}
          component={prop.component}
          key={key}
          exact={prop.exact}
          roles={prop.roles ? prop.roles : null}
        />
      );
    })}
    <Route exact path='/access-denied' component={AccessDenied} />
  </Switch>
);

export function Approver() {
  useInjectReducer({ key: 'Approver', reducer });
  useInjectSaga({ key: 'Approver', saga });

  const approver = useSelector(makeSelectApprover());
  const [routes, setRoutes] = useState(approverRoutes);


  const { currentUser, miniActive, changePass } = approver;
  useEffect(() => {
    if (currentUser) {
      const { roles: userRoles } = currentUser;
      // const userRoles = ['USER'];
      const newRoutes = [];
      for (let i = 0; i < approverRoutes.length; i += 1) {
        const route = approverRoutes[i];
        // if (route.roles && route.roles.length > 0) {
        //   if (isInRole(userRoles, route.roles)) newRoutes.push(route);
        // } else {
        newRoutes.push(route);
        // }

        route.views =
          route.views &&
          route.views.filter(view => {
            // if (view.roles && view.roles.length > 0) {
            //   return isInRole(userRoles, view.roles);
            // }
            return true;
          });
      }
      setRoutes(newRoutes);
    }
  }, [currentUser]);
  return (
    <div>
      <Helmet>
        <title>Approver</title>
        <meta name="description" content="Description of Approver" />
      </Helmet>
      <Header />
      <SideBar routes={routes} />
      <div>
        <div >{switchRoutes(routes)}</div>
      </div>
      <Footer />
    </div>
  );
}

// Approver.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// };

// const mapStateToProps = createStructuredSelector({
//   Approver: makeSelectApprover(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// );

// export default compose(
//   withConnect,
//   memo,
// )(Approver);

export default memo(Approver);

