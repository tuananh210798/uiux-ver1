import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the approver state domain
 */

const selectApproverDomain = state => state.approver || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Approver
 */

const makeSelectApprover = () =>
  createSelector(
    selectApproverDomain,
    substate => substate,
  );

export default makeSelectApprover;
export { selectApproverDomain };
