/**
 *
 * Header
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import LocaleToggle from 'containers/LocaleToggle';
import { useDispatch } from 'react-redux';
import { changeLoginStatus } from 'containers/App/actions';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { clearStorage } from '../../../utils/storage';

function Header() {
  const dispatch = useDispatch();
  const logout = () => {
    clearStorage();
    dispatch(changeLoginStatus(false));
  };
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu">
          <MenuIcon />
        </IconButton>
        <Typography variant="h6">
          <FormattedMessage id="app.components.Header.title" />
        </Typography>
        <FormattedMessage id="app.components.Header.hello" values={{ username: 'Quy' }} />
        <LocaleToggle />
        <Button className="acb" color="inherit" onClick={() => logout()}>LogOut</Button>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {};

export default memo(Header);
