/**
 *
 * SideBar
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import {
  withStyles,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Hidden,
  Collapse,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { makeSelectLocation } from 'containers/App/selectors';
import { NavLink, useLocation, Link } from 'react-router-dom';

function SideBar({ routes }) {
  const dispatch = useDispatch();
  const routerLocation = useSelector(makeSelectLocation());
  const activeRoute = (routeName) => {
    return routerLocation.pathname.indexOf(routeName) > -1;
  }

  const openCollapse = (collapse) => {
    const st = {};
    st[collapse] = !this.state[collapse];
    this.setState(st);
  }
  // Link
  const links = (
    <List >
      {routes.map((prop, index) => {
        // if (prop.redirect || prop.hide) {
        //   return null;
        // }
        if (prop.collapse) {
          // const navLinkClasses = `${classes.itemLink} ${cx({
          //   [` ${classes.collapseActive}`]: this.activeRoute(prop.path),
          // })}`;
          // const itemText = `${classes.itemText} ${cx({
          //   [classes.itemTextMini]:
          //     this.props.miniActive && this.state.miniActive,
          //   [classes.itemTextMiniRTL]:
          //     rtlActive && this.props.miniActive && this.state.miniActive,
          //   [classes.itemTextRTL]: rtlActive,
          // })}`;
          // const collapseItemText = `${classes.collapseItemText} ${cx({
          //   [classes.collapseItemTextMini]:
          //     this.props.miniActive && this.state.miniActive,
          //   [classes.collapseItemTextMiniRTL]:
          //     rtlActive && this.props.miniActive && this.state.miniActive,
          //   [classes.collapseItemTextRTL]: rtlActive,
          // })}`;
          // const itemIcon = `${classes.itemIcon} ${cx({
          //   [classes.itemIconRTL]: rtlActive,
          // })}`;
          // const caret = `${classes.caret} ${cx({
          //   [classes.caretRTL]: rtlActive,
          // })}`;
          return (
            <ListItem key={`list_${index}`} >
              {!prop.path ? (
                <div
                  onClick={() => openCollapse(prop.state)}
                  style={{ cursor: 'pointer' }}
                >
                  <ListItemIcon style={{ marginRight: '0' }}>
                    {/* <prop.icon /> */}
                  </ListItemIcon>
                  <ListItemText
                    primary={prop.name.toUpperCase()}
                    // secondary={
                    //   <b
                    //     className={`${caret} ${this.state[prop.state] ? classes.caretActive : ''
                    //       }`}
                    //   />
                    // }
                    disableTypography
                  // className={itemText}
                  />{activeRoute(prop.path) ? '1' : '2'}
                </div>
              ) : (
                  <NavLink
                    to={prop.path}
                    // className={navLinkClasses}
                    key={`nav_${index}`}
                    onClick={() => this.openCollapse(prop.state)}
                  >
                    <ListItemIcon style={{ marginRight: '0' }}>
                      {/* <prop.icon /> */}
                    </ListItemIcon>
                    <ListItemText
                      primary={prop.name.toUpperCase()}
                      // secondary={
                      //   <b
                      //     className={`${caret} ${this.state[prop.state] ? classes.caretActive : ''
                      //       }`}
                      //   />
                      // }
                      disableTypography
                    // className={itemText}
                    />{activeRoute(prop.path) ? '1' : '2'}
                  </NavLink>
                )}

              <Collapse
                key={`collapse_${index}`}
                in={true}
                unmountOnExit
              >
                <List >
                  {prop.views.map((view, i) => {
                    if (view.redirect || view.hide) {
                      return null;
                    }
                    // const navLinkClasses = `${classes.collapseItemLink} ${cx({
                    //   [` ${classes[color]}`]: this.activeRoute(view.path),
                    // })}`;
                    // const collapseItemMini = `${classes.collapseItemMini
                    //   } ${cx({
                    //     [classes.collapseItemMiniRTL]: rtlActive,
                    //   })}`;
                    return (
                      <ListItem
                        key={`listItem_${i}`}
                      // className={classes.collapseItem}
                      >
                        <NavLink to={view.path} >
                          <span >
                            {view.mini}
                          </span>
                          <ListItemText
                            primary={view.name}
                            disableTypography
                          // className={collapseItemText}
                          />{activeRoute(prop.path) ? '1' : '2'}
                        </NavLink>
                      </ListItem>
                    );
                  })}
                </List>
              </Collapse>
            </ListItem>
          );
        }
        // const navLinkClasses = `${classes.itemLink} ${cx({
        //   [` ${classes[color]}`]: this.activeRoute(prop.path),
        // })}`;
        // const itemText = `${classes.itemText} ${cx({
        //   [classes.itemTextMini]:
        //     this.props.miniActive && this.state.miniActive,
        //   [classes.itemTextMiniRTL]:
        //     rtlActive && this.props.miniActive && this.state.miniActive,
        //   [classes.itemTextRTL]: rtlActive,
        // })}`;
        // const itemIcon = `${classes.itemIcon} ${cx({
        //   [classes.itemIconRTL]: rtlActive,
        // })}`;
        return (
          <ListItem key={`l_${index}`}>
            <Link to={prop.path}
            // className={navLinkClasses}
            >
              <ListItemIcon style={{ marginRight: '0' }}>
                {/* <prop.icon /> */}
              </ListItemIcon>
              <ListItemText

                primary={<FormattedMessage id={prop.name} />}
              // disableTypography
              // className={itemText}
              />{activeRoute(prop.path) ? '1' : '2'}
            </Link>
          </ListItem>
        );
      })}
    </List>
  );
  return (
    <>
      { links}
    </>
  );
}

SideBar.propTypes = {};

export default memo(SideBar);
