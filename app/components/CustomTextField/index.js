/* eslint-disable prettier/prettier */
import React, { memo } from 'react';
import { TextField, InputAdornment, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  input: {
    color: 'rgba(0, 0, 0, 0.87) !important',
  },
}));

function CustomTextField(props) {
  const { error, endIcon, startIcon, required, value, onChange, readOnly, focused, ...rest } = props;
  const classes = useStyles();
  const trimStartValue = e => {
    if (e && e.target && e.target.value.trimStart) {
      e.target.value = e.target.value.trimStart();
    }
    onChange(e);
  }
  const inputClasses = readOnly ? classes.input : null;
  return <TextField
    focused={focused}
    InputProps={{
      className: inputClasses,
      endAdornment: endIcon && (
        <InputAdornment position="end">
          {endIcon}
        </InputAdornment>
      ),
      startAdornment: startIcon && (
        <InputAdornment position="start">
          {startIcon}
        </InputAdornment>
      ),
    }}
    disabled={readOnly}
    required={required}
    value={value}
    onChange={trimStartValue}
    error={required ? value.length === 0 : !!props.error}
    {...rest}
  />;
}
export default memo(CustomTextField);

CustomTextField.defaultProps = {
  variant: 'outlined',
  margin: 'dense',
  size: 'small',
  fullWidth: true,
  value: '',
  required: false,
  error: false,
  readOnly: false,
};

CustomTextField.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  fullWidth: PropTypes.bool,
  onChange: PropTypes.func,
  margin: PropTypes.string,
  variant: PropTypes.string,
  startIcon: PropTypes.object,
  endIcon: PropTypes.object,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  required: PropTypes.bool,
  error: PropTypes.any,
  readOnly: PropTypes.bool,
  focused: PropTypes.bool,
}